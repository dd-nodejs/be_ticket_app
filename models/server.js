const cors = require('cors');
const { response } = require('express');
const express = require('express');
const http = require('http');
const path = require('path');
const socketio = require('socket.io');

const Socket = require('./socket');

class Server {
  constructor() {
    // ? Servidor de Express
    this.app = express();
    this.port = process.env.PORT;

    // ? HTTP Server
    this.server = http.createServer(this.app);

    // ? Configuración del socket server
    this.io = socketio(this.server, {
      /* ...socket server configs */
    });

    this.sockets = new Socket(this.io);
  }

  middlewares() {
    // ? Desplegar el directorio publico
    this.app.use(express.static(path.resolve(__dirname, '../public')));

    // ? CORS
    this.app.use(cors());

    // GET de los últimos tickets
    this.app.get('/ultimos', (req, res = response) => {
      res.json({
        success: true,
        tickets: this.sockets.ticketList.ultimos13
      });
    });
  }

  execute() {
    // ? Inicializar middlewares
    this.middlewares();

    // ? Inicializar el servidor de express
    this.server.listen(this.port, () => {
      console.log(`Servidor corriendo en puerto: ${this.port}`);
    });
  }
}

module.exports = Server;
