const Ticket = require('./ticket');

class TicketList {
  constructor() {
    this.ultimoTurno = 0;
    this.pendientes = [];
    this.asignados = [];
  }

  get siguienteTurno() {
    this.ultimoTurno++;
    return this.ultimoTurno;
  }

  // ? 3 Tickets para las tarjetas y 10 en historial
  get ultimos13() {
    return this.asignados.slice(0, 13);
  }

  crearTicket() {
    const nuevoTicket = new Ticket(this.siguienteTurno);

    this.pendientes = [...this.pendientes, nuevoTicket];
    this.ultimoTurno = this.ultimoTurno++;

    return nuevoTicket;
  }

  asignarTicket(agente, escritorio) {
    if (this.pendientes.length === 0) {
      return null;
    }

    const siguienteTicket = this.pendientes.shift();
    siguienteTicket.agente = agente;
    siguienteTicket.escritorio = escritorio;

    this.asignados = [siguienteTicket, ...this.asignados];

    return siguienteTicket;
  }
}

module.exports = TicketList;
