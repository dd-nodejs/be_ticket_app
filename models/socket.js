const TicketList = require('./ticket-list');

class Socket {
  constructor(io) {
    this.io = io;

    // ? Crear instancia de ticket-list
    this.ticketList = new TicketList();

    this.socketEvents();
  }

  socketEvents() {
    this.io.on('connection', socket => {
      socket.on('solicitar-ticket', (data, callback) => {
        console.log('Solicitar ticket called');
        const nuevoTicket = this.ticketList.crearTicket();
        callback(nuevoTicket);
      });

      socket.on(
        'asignar-siguiente-ticket',
        ({ agente, escritorio }, callback) => {
          const ticketAsignado = this.ticketList.asignarTicket(
            agente,
            escritorio
          );

          callback(ticketAsignado);

          this.io.emit('ticket-asignado', this.ticketList.ultimos13);
        }
      );
    });
  }
}

module.exports = Socket;
