const { v4: uuidv4 } = require('uuid');

class Ticket {
  constructor(turno) {
    this.id = uuidv4();
    this.turno = turno;
    this.escritorio = null;
    this.agente = null;
  }
}

module.exports = Ticket;
