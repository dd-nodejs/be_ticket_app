# Tickets App
## Instructions

Install node packages.
```
npm install
```

Copy **.env.example** and rename it as **.env**. You must use this file as a template for the required enviroment variables.
<br/>
- Add PORT variable at .env, to use it as endpoint:

```
PORT=8080
```



Start nodeJS server.
```
npm run dev
```
- ***nodemon*** command only works if you have been installed the package

 